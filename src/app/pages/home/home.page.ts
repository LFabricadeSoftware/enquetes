import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { User } from '../../interfaces/user';
import { Cupom } from 'src/app/interfaces/cupom';
import { Subscription } from 'rxjs';
import { CuponsService } from 'src/app/services/cupons.service';
import { UserService } from 'src/app/services/user.service';
import { LoadingController, ToastController, NavController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  private loading: any;
  public cupons: Cupom = {};
  private cuponsSubscription: Subscription;

  private userId: string;
  private idCupom: any;
  private cupom: any;
  public qntCupons: any;
  public qntCuponsUser: number = 0;

  public user: User = {};
  public usuarios: User = {};
  //private usuarios: User = {};
  private Iduser: string;
  private phoneNumber: string;
  private userSubscription: Subscription;
  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private authService: AuthService,
    private cuponsService: CuponsService,
    private userService: UserService,
    private navCtrl: NavController,
  ) {
    this.getUser();
    
    this.cuponsSubscription = this.cuponsService.getCupons().subscribe(data => {
      for (var x = 0; x < data.length; x++) {
        //console.log(data[x].vigente);
        //console.log(data[x].idEnquete);
        if (data[x].userId == this.userId) {
          this.cupons = data[x];
          this.idCupom = data[x].id;
          this.qntCuponsUser++;
          console.log(this.qntCuponsUser);
        }
      }
    });
  }
  ngOnInit() {
  }
  log() {
    return this.user.email;
  }
  async getUser() {
    this.phoneNumber = (await this.authService.getAuth().currentUser).phoneNumber;
    //this.user.phoneNumber =  (await this.authService.getAuth().currentUser).phoneNumber;
    this.userId = (await this.authService.getAuth().currentUser).uid;
    //console.log("1")
    this.userSubscription = this.userService.getUsuarios().subscribe(data => {
      for (let x = 0; x < data.length; x++) {
        if (data[x].id == this.userId) {
          this.user = data[x];
          this.usuarios = data[x];
          this.Iduser = data[x].id;
          //console.log(this.usuarios.profissao+ " " + this.usuarios.id)
        } else {
          this.usuarios.phoneNumber = this.phoneNumber;
          console.log(this.usuarios.phoneNumber + this.usuarios.id + "13")
        }
      }
      if(!this.Iduser){
        alert("complete seu login");
        this.navCtrl.navigateBack('/perfil');
      }
    });
  }
  async getCupons() {
    console.log(this.idCupom);
    this.cuponsSubscription = this.cuponsService.getCupom(this.idCupom).subscribe(data => {
      this.cupom = data;
      //this.cupons.qntCupons = this.cupons.qntCupons;
    });
  }
  async presentLoading() {
    this.loading = await this.loadingCtrl.create({ message: 'Por favor aguarde!' });
    return this.loading.present();
  }
  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 2000 });
    toast.present();
  }
}
