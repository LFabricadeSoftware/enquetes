import { Component, OnInit } from '@angular/core';
import { Enquete } from 'src/app/interfaces/enquete';
import { Voto } from 'src/app/interfaces/voto';
import { EnqueteService } from 'src/app/services/enquete.service';
import { VotoService } from 'src/app/services/voto.service';
import { CuponsService } from 'src/app/services/cupons.service';
import { LoadingController, ToastController, NavController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute, UrlTree } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { Cupom } from 'src/app/interfaces/cupom';
import { ContVotoService } from 'src/app/services/cont-voto.service';
import { ContVoto } from 'src/app/interfaces/cont-voto';
import { User } from 'src/app/interfaces/user';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-enquete-semanal',
  templateUrl: './enquete-semanal.page.html',
  styleUrls: ['./enquete-semanal.page.scss'],
})
export class EnqueteSemanalPage implements OnInit {
  private disable: boolean;
  private loading: any;
  //voto
  private votoId: string = null;
  private idVoto: string = null;
  public voto: Voto = {};
  private votoSubscription: Subscription;
  //enquete
  //private enqueteId: string = null;
  public idEnquete: string;
  public enquete: Enquete = {};
  public enquetes = new Array<Enquete>();
  //public enquetes: Enquete = {};
  public enquetesArray = new Array<Enquete>();
  private enquetesSubscription: Subscription;
  //cupom
  private cupomId: string = null;
  private idCupom: string = null;
  public cupom: Cupom = {};
  private cuponsSubscription: Subscription;
  //qntVotos
  private contVotoId: string = null;
  private idContVoto: string = null;
  public contVoto: ContVoto = {};
  private contVotosSubscription: Subscription;

  private idUser: string;
  date: any;
  month: any;
  year: any;
  day: any;
  private currentDate: Date;
  //private fb: any;
  public usuarios: User = {};
  private userId: string;
  public user: User = {};
  //private usuarios: User = {};
  private Iduser: string;
  private phoneNumber: string;
  private userSubscription: Subscription;
  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private authService: AuthService,
    private activateRoute: ActivatedRoute,
    private votosService: VotoService,
    private enquetesService: EnqueteService,
    private cuponsService: CuponsService,
    private contVotoService: ContVotoService,
    private navCtrl: NavController,
    private userService: UserService,
  ) {
    this.votoId = this.activateRoute.snapshot.params['id'];

    this.getUser();
    if (this.votoId) this.loadVoto();
    this.date = new Date();
    this.formatDate(this.date);
    this.currentDate = this.date;
    console.log(this.currentDate);
    //this.enquetesSubscription = this.enquetesService.getEnquetes().subscribe(data => {
    //  this.enquetes = data;
    //Enquetes
    this.enquetesSubscription = this.enquetesService.getEnquetes().subscribe(data => {
      this.enquetes = data;
      //console.log("123456789876543456789");
      for (var x = 0; x < this.enquetes.length; x++) {
        //console.log(data[x].vigente);
        //var myBool: boolean = data.vigente;
        //var myString: string = String(myBool);
        //console.log(myString)
        //alert(myString);
        //const vigente: string = bool.toString(data[x].vigente);
        //console.log(vigente + "6r28y4j534");
        //Comparar strings ou boolean
        //this.enquete = this.enquetes[x];
        //alert(this.currentDate);
        let stringI = this.enquetes[x].dataInicio.toString();
        let arrI = stringI.split("-", 3);
        //alert(arrI);
        let stringF = this.enquetes[x].dataFinal.toString();
        let arrF = stringF.split("-", 3);
        //alert(arrF);
        let stringD = this.currentDate.toString();
        let arrD = stringD.split("-", 3);
        //alert(arrD);
        if (arrI[0] == arrD[0] && arrF[0] == arrD[0]) {
          //alert(arrI[0] + "-" + arrD[0] + "-" + arrF[0]);
          if (arrI[1] == arrD[1] &&   arrD[1] == arrF[1] ) {
            //alert(arrI[1] + "-" + arrF[1] + "-" + arrD[1] + "--" + arrI[2] + "-" + arrD[2] + "-" + arrF[2]);
            //alert(arrI[2] +"-" + arrD[2] + "-" + arrF[2])
            if (arrI[2] <= arrD[2] && arrF[2] >= arrD[2]) {
              //alert(arrI[2] + "-" + arrD[2] + "-" + arrF[2]);
              this.enquete = this.enquetes[x];
              this.idEnquete = data[x].id;
            }
          }
        }
        //alert(this.enquetes[x].dataInicio.getUTCDate);
        //console.log(this.enquetes[x].dataInicio + " " + this.currentDate + " " + this.enquetes[x].dataFinal)
        /*if(this.enquetes[x].dataInicio.getDay > this.currentDate.getDay && this.enquetes[x].dataFinal.getDay < this.currentDate.getDay){
           //console.log("1");
           console.log("1234567890")
           this.enquete = this.enquetes[x];
          }else{
            //console.log("2");
          }*/
         // console.log(vigente);
      
        //console.log();
        //if(myString === "true"){
         // console.log(data[x].vigente);
          //this.enquete = data[x];
          
          //console.log(this.enquete);
        //}
      }
      //this.enquetes.
      //var myBool: boolean = data;
        //var myString: string = String(myBool);
      
    });
    /*this.enquetesSubscription = this.enquetesService.getEnquetes().subscribe(data => {
      for(var x = 0; x < data.length; x++){
        //console.log(data[x].vigente);
        var myBool: boolean = data[x].vigente;
        var myString: string = String(myBool);
        //alert(myString);
        //const vigente: string = bool.toString(data[x].vigente);
        //console.log(vigente + "6r28y4j534");
        //Comparar strings ou boolean
        if(myString === 'true'){
           //console.log("1");
          }else{
            //console.log("2");
          }
         // console.log(vigente);
      
        //console.log();
        //if(myString === "true"){
         // console.log(data[x].vigente);
          this.enquetes = data[x];
          this.idEnquete = data[x].id;
          //console.log(this.enquete);
        //}
      }
      
    });*/
    //alert(this.userId)
        this.votoSubscription = this.votosService.getVotos().subscribe(data => {
          for (var x = 0; x < data.length; x++) {
            //console.log(data[x].vigente);
            if (data[x].enqueteId == this.idEnquete && data[x].userId == this.userId) {
              //console.log(data[x]);
              this.voto = data[x];
              this.idVoto = data[x].id;
              //console.log(this.idEnquete);
            }
          }

        });
        this.cuponsSubscription = this.cuponsService.getCupons().subscribe(data => {
          for (var x = 0; x < data.length; x++) {
            //console.log(data[x].vigente);
            this.getUsuario();
            console.log("Este é o usuário:" + this.idUser);
            if (data[x].userId != this.idUser) {
              console.log(data[x].userId);
              this.cupom = data[x];
              this.idCupom = data[x].id;
              //return this.enquete;
              //console.log(this.idEnquete);
            }
          }

        });
        this.contVotosSubscription = this.contVotoService.getContVotos().subscribe(data => {
          for (var x = 0; x < data.length; x++) {
            //console.log(data[x].vigente);
            if (data[x].idEnquete == this.idEnquete) {
              //console.log(data);
              this.contVoto = data[x];
              this.idContVoto = data[x].id;
              //return this.enquete;
              //console.log(this.idEnquete);
            }
          }

        });
      }

      ngOnInit() {
        
      }
      ngOnDestroy(){
        if (this.votoSubscription) this.votoSubscription.unsubscribe();
        if (this.enquetesSubscription) this.enquetesSubscription.unsubscribe();
        if (this.cuponsSubscription) this.cuponsSubscription.unsubscribe();
        if (this.contVotosSubscription) this.contVotosSubscription.unsubscribe();
      }

      async salvarVoto(){
        await this.presentLoading();

        if (this.voto.enqueteId == this.idEnquete && this.voto.userId == (await this.authService.getAuth().currentUser).uid) {
          this.presentToast('Você já votou');
          //await this.loading.dismiss();
          //this.navCtrl.navigateRoot('/res-enquete');
        } else {
          this.voto.userId = (await this.authService.getAuth().currentUser).uid;
          this.voto.enqueteId = (await this.idEnquete);
          await this.votosService.addVoto(this.voto);

          //console.log((await this.authService.getAuth().currentUser).uid, this.idEnquete);
          //voto

          //cupons
          //if(this.idCupom == null || this.cupom.userId != (await this.authService.getAuth().currentUser).uid){
          this.cupom.qntCupons = 1;
          this.cupom.userId = (await this.authService.getAuth().currentUser).uid;
          await this.cuponsService.addCupom(this.cupom);


          //}else{
          //  this.cupom.qntCupons += 1;
          //  console.log(typeof(this.cupom.qntCupons) + " - " +this.cupom.id);
          //   await this.cuponsService.updateCupom(this.cupom.id, this.cupom);
          //  console.log("Este cupom é o " + this.cupom);
          //  console.log(typeof(this.cupom.qntCupons));
          //await this.loading.dismiss();
          // }
          //contVoto
          //alert(this.voto.voto);
          if (this.idContVoto == null) {
            this.contVoto.votosA = 0;
            this.contVoto.votosB = 0;
            this.contVoto.votosC = 0;
            this.contVoto.votosD = 0;
            this.contVoto.votosE = 0;
            switch (this.voto.voto) {
              case "1":
                const val1 = 1;
                this.contVoto.votosA = 1;
                break;
              case "2":
                const val2 = 1;
                this.contVoto.votosB = 1;
                break;
              case "3":
                const val3 = 1;
                this.contVoto.votosC = 1;
                break;
              case "4":
                const val4 = 1;
                this.contVoto.votosD = 1;
                break;
              case "5":
                const val5 = 1;
                this.contVoto.votosE = 1;
                break;
              default:
                break;
            }
            //this.contVoto.votosA = 1;
            //this.contVoto.idEnquete = (await this.idEnquete);
            this.contVoto.idEnquete = (await this.idEnquete);
            await this.contVotoService.addContVoto(this.contVoto);
            console.log(this.contVoto);
            this.cupom.qntCupons += 1;
            //await this.loading.dismiss();
          } else {
            switch (this.voto.voto) {
              case "1":
                //const val1 = 1;
                this.contVoto.votosA++;
                break;
              case "2":
                //const val2 = 1;
                this.contVoto.votosB++;
                break;
              case "3":
                //const val3 = 1;
                this.contVoto.votosC++;
                break;
              case "4":
                //const val4 = 1;
                this.contVoto.votosD++;
                break;
              case "5":
                //const val5 = 1;
                this.contVoto.votosE++;
                break;
              default:
                break;
            }
            //this.contVoto.votosA = 1;
            this.contVoto.idEnquete = (await this.idEnquete);
            await this.contVotoService.updateContVoto(this.contVoto.id, this.contVoto);
            console.log(this.contVoto);
            //await this.loading.dismiss();

          }

        }
        await this.loading.dismiss();
        //this.navCtrl.navigateRoot('/res-enquete', { this.idEnquete });
      }

      async getUser() {
        //this.phoneNumber = (await this.authService.getAuth().currentUser).phoneNumber;
        //this.user.phoneNumber =  (await this.authService.getAuth().currentUser).phoneNumber;
        this.userId = (await this.authService.getAuth().currentUser).uid;
        //console.log("1")
        this.userSubscription = this.userService.getUsuarios().subscribe(data => {
          for (let x = 0; x < data.length; x++) {
            if (data[x].id == this.userId) {
              this.user = data[x];
              this.usuarios = data[x];
              this.Iduser = data[x].id;
              //console.log(this.usuarios.profissao+ " " + this.usuarios.id)
            } else {
              this.usuarios.phoneNumber = this.phoneNumber;
              console.log(this.usuarios.phoneNumber + this.usuarios.id + "13")
            }
          }
        });
      }
      async loadVoto(){

        //var recUserId = (await this.authService.getAuth().currentUser).uid;
        //var recEnqueteId = (await this.idEnquete);

        //console.log("Aquei estão os valores");
        //if(this.voto.userId == recUserId && this.voto.userId == recEnqueteId){
        //  this.votoSubscription = this.votosService.getVoto(this.votoId).subscribe(data => {
        //    this.voto = data;
        //    console.log(this.voto);



        //  });
        //}

      }

      //async loadEnquete(){
      //  this.enquetesSubscription = this.enquetesService.getEnquete(this.idEnquete).subscribe(data => {
      //    this.enquete = data;
      //  });
      //}

      async getUsuario(){
        this.idUser = (await this.authService.getAuth().currentUser).uid;
      }

      doRefresh(event) {
        console.log('Begin async operation');

        setTimeout(() => {
          console.log('Async operation has ended');
          event.target.complete();
        }, 2000);
      }

      async formatDate(date) {
        var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

        if (month.length < 2)
          month = '0' + month;
        if (day.length < 2)
          day = '0' + day;
        this.date = [year, month, day].join('-');
        console.log(this.date);
        //return [year, month, day].join('-');
      }
      async presentLoading() {
        this.loading = await this.loadingCtrl.create({ message: 'Por favor aguarde!' });
        return this.loading.present();
      }
      async presentToast(message: string) {
        const toast = await this.toastCtrl.create({ message, duration: 2000 });
        toast.present();
      }
    }
