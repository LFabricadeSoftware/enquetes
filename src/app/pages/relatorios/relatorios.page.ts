import { User } from './../../interfaces/user';
import { Component, OnInit } from '@angular/core';
import 'chart.js';
import { Chart } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { Subscription } from 'rxjs';
import { ActivatedRoute, UrlTree } from '@angular/router';
import { Enquete } from 'src/app/interfaces/enquete';
import { Voto } from 'src/app/interfaces/voto';
import { LoadingController, ToastController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { EnqueteService } from 'src/app/services/enquete.service';
import { VotoService } from 'src/app/services/voto.service';
import { CuponsService } from 'src/app/services/cupons.service';
import { ContVotoService } from 'src/app/services/cont-voto.service';
import { ContVoto } from 'src/app/interfaces/cont-voto';


@Component({
  selector: 'app-relatorios',
  templateUrl: './relatorios.page.html',
  styleUrls: ['./relatorios.page.scss'],
  //directives: [BaseChartDirective]
})
export class RelatoriosPage implements OnInit {
  public idEnquete: string;
  public enquetes = new Array<Enquete>();
  public enquete = {};
  private enquetesSubscription: Subscription;
  private loading: any;

  public idContVoto: string;
  public idContVotoEnquete: string;
  public contVotos = new Array<ContVoto>();
  public contVoto: ContVoto = {};
  private contVotosSubscription: Subscription;

  public usuarios: User = {};
  private userId: string;
  public user: User = {};
  //private usuarios: User = {};
  private Iduser: string;
  private phoneNumber: string;
  private userSubscription: Subscription;
  
  private votoId: string = null;
  private idVoto: string = null;
  public voto: Voto = {};
  private votoSubscription: Subscription;
  private idUser: string;
  date: any;
  month: any;
  year: any;
  day: any;
  private currentDate: Date;
  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private authService: AuthService,
    private activateRoute: ActivatedRoute,
    private votosService: VotoService,
    private enquetesService: EnqueteService,
    private cuponsService: CuponsService,
    private contVotoService: ContVotoService,
    private navCtrl: NavController,
    private userService: UserService,
  ) {
    this.enquetesSubscription = this.enquetesService.getEnquetes().subscribe(data => {
      this.enquetes = data;
      //console.log("123456789876543456789");
      for (var x = 0; x < this.enquetes.length; x++) {
        //console.log(data[x].vigente);
        //var myBool: boolean = data.vigente;
        //var myString: string = String(myBool);
        //console.log(myString)
        //alert(myString);
        //const vigente: string = bool.toString(data[x].vigente);
        //console.log(vigente + "6r28y4j534");
        //Comparar strings ou boolean
        //this.enquete = this.enquetes[x];
        //alert(this.currentDate);
        let stringI = this.enquetes[x].dataInicio.toString();
        let arrI = stringI.split("-", 3);
        //alert(arrI);
        let stringF = this.enquetes[x].dataFinal.toString();
        let arrF = stringF.split("-", 3);
        //alert(arrF);
        let stringD = this.currentDate.toString();
        let arrD = stringD.split("-", 3);
        //alert(arrD);
        if (arrI[0] == arrD[0] && arrF[0] == arrD[0]) {
          //alert(arrI[0] + "-" + arrD[0] + "-" + arrF[0]);
          if (arrI[1] == arrD[1] &&   arrD[1] == arrF[1] ) {
            //alert(arrI[1] + "-" + arrF[1] + "-" + arrD[1] + "--" + arrI[2] + "-" + arrD[2] + "-" + arrF[2]);
            //alert(arrI[2] +"-" + arrD[2] + "-" + arrF[2])
            if (arrI[2] <= arrD[2] && arrF[2] >= arrD[2]) {
              //alert(arrI[2] + "-" + arrD[2] + "-" + arrF[2]);
              this.enquete = this.enquetes[x];
              this.idEnquete = data[x].id;
            }
          }
        }
        //alert(this.enquetes[x].dataInicio.getUTCDate);
        //console.log(this.enquetes[x].dataInicio + " " + this.currentDate + " " + this.enquetes[x].dataFinal)
        /*if(this.enquetes[x].dataInicio.getDay > this.currentDate.getDay && this.enquetes[x].dataFinal.getDay < this.currentDate.getDay){
           //console.log("1");
           console.log("1234567890")
           this.enquete = this.enquetes[x];
          }else{
            //console.log("2");
          }*/
         // console.log(vigente);
      
        //console.log();
        //if(myString === "true"){
         // console.log(data[x].vigente);
          //this.enquete = data[x];
          
          //console.log(this.enquete);
        //}
      }
      //this.enquetes.
      //var myBool: boolean = data;
        //var myString: string = String(myBool);
      
    });
   }

  /*public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];*/
  ngOnInit() {
    this.showChart();
  }
  contEnquete(){
    alert(this.idContVotoEnquete);
    this.enquetesSubscription = this.enquetesService.getEnquete(this.idContVotoEnquete).subscribe(data => {
      this.contVoto = data;
      alert(this.contVoto);
      this.contVoto.votosA
      
    });
  }
  showChart(){
    var ctx = (<any>document.getElementById('myChart')).getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'pie',
      data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
          label: '# of Votes',
          data: [10,45,23,12],//this.contVoto.votosA, this.contVoto.votosB, this.contVoto.votosC, this.contVoto.votosD, this.contVoto.votosE
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }
}
