export interface ContVoto {
    id?: string;
    votosA?: number;
    votosB?: number;
    votosC?: number;
    votosD?: number;
    votosE?: number;
    idEnquete?: string;
}
