export interface Voto {
    id?: string,
    voto?: string,
    enqueteId?: string,
    userId?: string,
}
