export interface Sorteio {
    id?: string;
    //minimo?: number;
    //maximo?: number;
    //somaV?: number;
    userId?: string;
    nomeGanhador?: string;
    data?: Date;
    premioId?: string;
}
