// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  /*firebase: {
    apiKey: "AIzaSyCE6-1NWLUTZISEBa9sa5ST9hhkcr7mjio",
    authDomain: "filozinhaapp.firebaseapp.com",
    databaseURL: "https://filozinhaapp.firebaseio.com",
    projectId: "filozinhaapp",
    storageBucket: "filozinhaapp.appspot.com",
    messagingSenderId: "668688635986",
    appId: "1:668688635986:web:ea4e83bc864e309355a78b",
    measurementId: "G-ZVK5HPECR5"
  }*/
  firebase : {
    apiKey: "AIzaSyCLKiUoaibKLjA_NhR_IbbnVAIHytqX-4I",
    authDomain: "filozinhaappprod.firebaseapp.com",
    databaseURL: "https://filozinhaappprod.firebaseio.com",
    projectId: "filozinhaappprod",
    storageBucket: "filozinhaappprod.appspot.com",
    messagingSenderId: "302271829397",
    appId: "1:302271829397:web:e941d12f1b527c4b7fd29c",
    measurementId: "G-5VT6FL48S6"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
